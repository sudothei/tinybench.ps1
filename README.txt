# tinybench

This script is:

- Tiny
- Fast
- Not totally accurate
- Only uses PowerShell

It is useful for:

- Extremely minimal performance reporting
- Lightweight benchmarking
- Not installing bloat

Records the last 30 runs by default, but can be changed by changing the
`runsKept` variable

Is designed to be combined with scheduling tools for running every
day/hour/whatever

Keeps the output in a file called `bench.csv`
