$runsKept = 30



function get-hash([string]$textToHash) {
    $hasher = new-object System.Security.Cryptography.MD5CryptoServiceProvider
    $toHash = [System.Text.Encoding]::UTF8.GetBytes($textToHash)
    $hashByteArray = $hasher.ComputeHash($toHash)
    foreach($byte in $hashByteArray)
    {
      $result += "{0:X2}" -f $byte
    }
    return $result;
 }

function get-cpu-speed() {
    $emptyBytes = '0' * 1mb
    $elapsedTime = Measure-Command { get-hash($emptyBytes) }
    $seconds = $elapsedTime.TotalSeconds
    $mbps =  [math]::Round(1 / $seconds, 2)
    return $mbps
}

function get-ram-speed() {
    $elapsedTime = Measure-Command { $emptyBytes = '0' * 1mb }
    $seconds = $elapsedTime.TotalSeconds
    $mbps =  [math]::Round(1 / $seconds, 2)
    return $mbps
}

function get-write-speed() {
    $emptyBytes = '0' * 1mb 
    $elapsedTime = Measure-Command {$emptyBytes > writeBench.txt}
    Remove-Item writeBench.txt
    $seconds = $elapsedTime.TotalSeconds
    $mbps =  [math]::Round(1 / $seconds, 2)
    return $mbps
}

function get-read-speed() {
    $emptyBytes = '0' * 1mb 
    $emptyBytes > readBench.txt
    $elapsedTime = Measure-Command {Get-Content 'readBench.txt'}
    Remove-Item readBench.txt
    $seconds = $elapsedTime.TotalSeconds
    $mbps =  [math]::Round(1 / $seconds, 2)
    return $mbps
}

function get-ping-speed() {
    $ping = ping -n 4 'charter.com'
    $ping = $ping[10] -replace "^.*Average = ", ""
    $ping = $ping -replace "ms$", ""
    return $ping
}

$date = Get-Date
$cpuSpeed = get-cpu-speed
$ramSpeed = get-ram-speed
$writeSpeed = get-write-speed
$readSpeed = get-read-speed
$pingSpeed = get-ping-speed

$results = @{
    "DATE" = $date
    "CPU" = "$cpuSpeed MB/s"
    "RAM" = "$ramSpeed MB/s"
    "WRITE" = "$writeSpeed MB/s"
    "READ" = "$readSpeed MB/s"
    "PING" = "$pingSpeed ms"
}

$results | ForEach-Object{ [pscustomobject]$_ } | Export-CSV bench.csv -NoTypeInformation -Append
$updatedFile = Import-CSV bench.csv | Select-Object -Last ($runsKept)
$updatedFile | Export-CSV -NoTypeInformation bench.csv
$updatedFile | Format-Table

